
package com.halem.sweply

import android.graphics.Typeface
import android.net.Uri
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.children
import androidx.databinding.BindingAdapter
import androidx.paging.PagedList
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.custom.sliderimage.logic.SliderImage
import com.google.android.material.textfield.TextInputLayout
import com.halem.sweply.adapters.CartAdapter
import com.halem.sweply.adapters.ItemsStoreAdapter

import com.halem.sweply.adapters.StoreAdapter
import com.halem.sweply.adapters.StoreItemsAdapater
import com.halem.sweply.fragments.store.model.DataItem
import com.halem.sweply.fragments.store.model.ImagesItem
import com.halem.sweply.fragments.store.model.LimitedProductsItem
import com.halem.sweply.utility.NetworkApiStatus


@BindingAdapter("arrayData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<DataItem>?) {
    val adapter = recyclerView.adapter as StoreAdapter
    adapter.submitList(data)
}

@BindingAdapter("itemsStore")
fun bindRecyclerViewSubItem(recyclerView: RecyclerView, data: List<LimitedProductsItem>?) {
    val adapter = recyclerView.adapter as StoreItemsAdapater
    adapter.submitList(data)
}

@BindingAdapter("detailStore")
fun bindRecyclerViewDetailStore(recyclerView: RecyclerView, data: PagedList<LimitedProductsItem>?) {
    val adapter = recyclerView.adapter as ItemsStoreAdapter
    adapter.submitList(data)
}
@BindingAdapter("cartData")
fun bindRecyclerCart(recyclerView: RecyclerView, data: List<LimitedProductsItem>?) {
    val adapter = recyclerView.adapter as CartAdapter
    adapter.submitList(data)
}

@BindingAdapter("refreshing")
fun swipping(swip: SwipeRefreshLayout, isLoading: Boolean) {
    swip.setColorSchemeResources(R.color.golden)
    swip.isRefreshing=isLoading
}

@BindingAdapter("font")
fun font(view: View, type: String) {
    val custom_Bold = Typeface.createFromAsset(view.context.assets, "fonts/Mada-Bold.ttf")
    val custom_med = Typeface.createFromAsset(view.context.assets, "fonts/Mada-Medium.ttf")
    val custom_reg = Typeface.createFromAsset(view.context.assets, "fonts/Mada-Regular.ttf")

    when(type){
        "bold"-> (view as TextView).typeface=custom_Bold
        "med"-> (view as TextView).typeface=custom_med
        "reg" ->(view as TextView).typeface=custom_reg
    }

}
@BindingAdapter("fontTextInputLayout")
fun fontTextInputLayout(view: View, type: String) {
    val custom_Bold = Typeface.createFromAsset(view.context.assets, "fonts/Mada-Bold.ttf")
    val custom_med = Typeface.createFromAsset(view.context.assets, "fonts/Mada-Medium.ttf")
    val custom_reg = Typeface.createFromAsset(view.context.assets, "fonts/Mada-Regular.ttf")

    when(type){
        "bold"-> (view as TextInputLayout).typeface=custom_Bold
        "med"-> (view as TextInputLayout).typeface=custom_med
        "reg" ->(view as TextInputLayout).typeface=custom_reg
    }

}
@BindingAdapter("fontToolbar")
fun fontToolBar(toolbar: Toolbar,s:String) {
    toolbar.changeToolbarFont()
}

fun Toolbar.changeToolbarFont(){
    for (i in 0 until childCount) {
        val view = getChildAt(i)
        if (view is TextView ) {
            view.typeface = Typeface.createFromAsset(view.context.assets, "fonts/Mada-Bold.ttf")
            break
        }
    }
}




@BindingAdapter("imageUrl")
fun bindImage(imgView: ImageView, imgUrl: ImagesItem?) {
    imgUrl?.let {
//        val imgUri = imgUrl.toUri().buildUpon().scheme("https").build()
        val imgUri = Uri.parse(imgUrl.img).buildUpon().scheme("https").build()
        Glide.with(imgView.context)
                .load(imgUri)
                .apply(RequestOptions()
//                        .placeholder(R.drawable.loading_animation)
                        .error(R.drawable.ic_broken_image))
                .into(imgView)
    }
}@BindingAdapter("sliderData")
fun bindSlider(imageSlider: SliderImage, images: List<ImagesItem>?) {
    images?.let {
         val imageUrls:ArrayList<String> = ArrayList()
        for ( x in images)
        { imageUrls.add(x.img) }
        imageSlider.setItems(imageUrls)
        imageSlider.addTimerToSlide(6000)
        imageSlider.gravity=Gravity.BOTTOM
    }
}


@BindingAdapter("networkStatus")
fun bindStatus(statusImageView: ImageView, status: NetworkApiStatus?) {
    when (status) {
        NetworkApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        NetworkApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        NetworkApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }


}
