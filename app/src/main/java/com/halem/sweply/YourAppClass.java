package com.halem.sweply;

import androidx.multidex.MultiDexApplication;

import com.onesignal.OneSignal;

public class YourAppClass extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }
}