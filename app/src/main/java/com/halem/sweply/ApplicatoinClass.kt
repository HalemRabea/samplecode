package com.halem.sweply

import android.app.Application
import androidx.multidex.MultiDexApplication
import com.onesignal.OneSignal

class ApplicatoinClass : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()

        // OneSignal Initialization
        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init()
    }
}