package com.halem.sweply

import android.view.animation.Interpolator
import android.view.animation.LinearInterpolator

class ReverseInterpolator @JvmOverloads constructor(private val delegate: Interpolator = LinearInterpolator()) :
    Interpolator {

    override fun getInterpolation(input: Float): Float {
        return 1 - delegate.getInterpolation(input)
    }
}