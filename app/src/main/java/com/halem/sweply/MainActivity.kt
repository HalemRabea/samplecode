package com.halem.sweply

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.app.ActionBar
import android.content.res.Configuration
import android.content.res.Resources
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import android.graphics.Bitmap
import android.graphics.Path
import android.os.Environment.getExternalStorageDirectory
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.os.Build
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.PathInterpolator
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.multidex.MultiDex
import com.bumptech.glide.load.engine.Resource
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.halem.sweply.dataBase.CartDatabase


class MainActivity : AppCompatActivity(),
    ViewPager.OnPageChangeListener,
    BottomNavigationView.OnNavigationItemReselectedListener,
    BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var  _notificationBadgeTextView:TextView
    // overall back stack of containers
    private val backStack = Stack<Int>()

    // list of base destination containers
    private val fragments = listOf(
        BaseFragment.newInstance(
            R.layout.content_stor_base,
            R.id.toolbar_store,
            R.id.nav_host_home
        ),
        BaseFragment.newInstance(R.layout.content_cart_base, R.id.toolbar_cart, R.id.nav_host_cart),
        BaseFragment.newInstance(
            R.layout.content_about_base,
            R.id.toolbar_about,
            R.id.nav_host_settings
        )
    )


    // map of navigation_id to container index
    private val indexToPage = mapOf(0 to R.id.home, 1 to R.id.library, 2 to R.id.settings)

    override fun onCreate(savedInstanceState: Bundle?) {
        MultiDex.install(this)
        setLanguage("ar")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // setup main view pager
        main_pager.addOnPageChangeListener(this)
        main_pager.adapter = ViewPagerAdapter()
        main_pager.post(this::checkDeepLink)
        main_pager.offscreenPageLimit = fragments.size

        // set bottom nav
        bottom_nav.setOnNavigationItemSelectedListener(this)
        bottom_nav.setOnNavigationItemReselectedListener(this)

        setNotifacationbadge()
        val database = CartDatabase.getInstance(application).itemDao
        val cartItems=database.getAllItems()

        cartItems.observe(this,androidx.lifecycle.Observer {
            if (it!=null){
                if (it.isNotEmpty()) {
                    _notificationBadgeTextView.visibility=View.VISIBLE
                    _notificationBadgeTextView.text = it.size.toString()
                }else _notificationBadgeTextView.visibility=View.INVISIBLE
            } else _notificationBadgeTextView.visibility=View.INVISIBLE
        })

        // initialize backStack with elements
        if (backStack.empty()) backStack.push(0)
    }

    fun setNotifacationbadge() {
        val notificationsTab = bottom_nav.findViewById<BottomNavigationItemView>(R.id.library)
        val badge = LayoutInflater.from(this)
            .inflate(R.layout.component_tabbar_badge, notificationsTab, false)
         _notificationBadgeTextView =
            badge.findViewById<TextView>(R.id.notificationsBadgeTextView)
        _notificationBadgeTextView.visibility=View.INVISIBLE
        notificationsTab.addView(badge)
    }

    /// BottomNavigationView ItemSelected Implementation
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val position = indexToPage.values.indexOf(item.itemId)
        if (main_pager.currentItem != position) setItem(position)
        return true
    }

    override fun onNavigationItemReselected(item: MenuItem) {
        val position = indexToPage.values.indexOf(item.itemId)
        val fragment = fragments[position]
        fragment.popToRoot()
    }

    override fun onBackPressed() {
        val fragment = fragments[main_pager.currentItem]
        val hadNestedFragments = fragment.onBackPressed()
        // if no fragments were popped
        if (!hadNestedFragments) {
            if (backStack.size > 1) {
                // remove current position from stack
                backStack.pop()
                // set the next item in stack as current
                main_pager.currentItem = backStack.peek()

            } else super.onBackPressed()
        }
    }

    /// OnPageSelected Listener Implementation
    override fun onPageScrollStateChanged(state: Int) {}

    override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {}

    override fun onPageSelected(page: Int) {
        val itemId = indexToPage[page] ?: R.id.home
        if (bottom_nav.selectedItemId != itemId) bottom_nav.selectedItemId = itemId
    }

    private fun setItem(position: Int) {
        main_pager.currentItem = position
//        backStack.push(position)
    }

    private fun checkDeepLink() {
        fragments.forEachIndexed { index, fragment ->
            val hasDeepLink = fragment.handleDeepLink(intent)
            if (hasDeepLink) setItem(index)
        }
    }

    inner class ViewPagerAdapter : FragmentPagerAdapter(supportFragmentManager) {

        override fun getItem(position: Int): Fragment = fragments[position]

        override fun getCount(): Int = fragments.size

    }

    private fun setLanguage(Language: String) {
        // Update language
        val locale = Locale(Language)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        this.resources.updateConfiguration(config, this.resources.displayMetrics)
    }

    fun takeScreenShot(view: View) {

        try {
            // image naming and path  to include sd card  appending name you choose for file

            // create bitmap screen capture
//            view = window.decorView.rootView
            view.isDrawingCacheEnabled = true
            val bitmap = Bitmap.createBitmap(view.drawingCache)
            view.isDrawingCacheEnabled = false

            animateView(view, bitmap)
        } catch (e: Throwable) {
            // Several error may come out with file handling or DOM
            e.printStackTrace()
        }

    }

    private fun animateView(view: View, b: Bitmap) {
        val mCheckoutImgView = iconToAnimate
        val scale = this.resources.displayMetrics.density
        val pixels = (80 * scale + 0.5f).toInt()

        animationView.setImageBitmap(b)
        animationView.visibility = View.VISIBLE
        val u = IntArray(2)
        mCheckoutImgView.getLocationInWindow(u)
        animationView.left = view.left
        animationView.top = view.top
        val animSetXY = AnimatorSet()
        val y = ObjectAnimator.ofFloat(
            animationView,
            "translationY",
//            animationView.top,
            (u[1] - 1 * pixels).toFloat()   //(u[1] - 2 * pixels).toFloat()       //mCheckoutImgView.bottom.toFloat()
        )
        val x = ObjectAnimator.ofFloat(
            animationView,
            "translationX",
//            animationView.left,
            (u[0] - 2 * pixels).toFloat()
        )
        val sy = ObjectAnimator.ofFloat(animationView, "scaleY", 0f)
        val sx = ObjectAnimator.ofFloat(animationView, "scaleX", 0f)
        animSetXY.playTogether(x, y, sx, sy)
        animSetXY.duration = 1100
        animSetXY.start()


        animSetXY.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                animationView.visibility = View.INVISIBLE
                animation.removeListener(this)
                animation.duration = 0
                animation.interpolator = ReverseInterpolator()
                animation.start()
//                super.onAnimationEnd(animation)
            }
        })

    }

}
