package com.halem.sweply.fragments.store.model


import android.os.Parcelable
import androidx.room.*
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

data class Status(@Json(name = "type")
                  var type: String? = "",
                  @Json(name = "title")
                  var title: String? = "")

@Parcelize
data class SizesItem(@Json(name = "size")
                     @ColumnInfo(name = "size_name") var size: String = " ",
                     @Json(name = "updated_at")
                     @Ignore var updatedAt: String = " ",
                     @Json(name = "product_id")
                     @ColumnInfo(name = "size_product_id") var productId: Int = 0,
                     @Json(name = "created_at")
                     @Ignore var createdAt: String = " ",
                     @Json(name = "id")
                     @ColumnInfo(name = "size_id") var id: Int = -1,
                     @Json(name = "colors")
                     @Ignore var colors: MutableList<ColorsItem>,
                     @Embedded var color: ColorsItem?
) : Parcelable{
    constructor() : this("","",0,"",0,mutableListOf<ColorsItem>(), ColorsItem())
}

@Parcelize
data class ImagesItem(@Json(name = "img")
                      var img: String = " ",
                      @Json(name = "updated_at")
                      @Ignore var updatedAt: String = " ",
                      @Json(name = "product_id")
                      @ColumnInfo(name = "image_product_id") var productId: Int = -1,
                      @Json(name = "created_at")
                      @Ignore var createdAt: String = " ",
                      @Json(name = "id")
                      @ColumnInfo(name = "image_id") var id: Int = -1) : Parcelable

@Parcelize
data class ColorsItem(@Json(name = "quantity")
                      var quantity: String = "0",
                      var choosenQuanity: String = "0",
                      @Json(name = "updated_at")
                      @Ignore var updatedAt: String = " ",
                      @Json(name = "product_id")
                      @ColumnInfo(name = "color_product_id")var productId: Int = -1,
                      @Json(name = "name")
                      @ColumnInfo(name = "color_name") var name: String = " ",
                      @Json(name = "size_id")
                      var sizeId: Int = -1,
                      @Json(name = "created_at")
                      @Ignore var createdAt: String = " ",
                      @Json(name = "hex")
                      var hex: String? = " ",
                      @Json(name = "id")
                      @ColumnInfo(name = "color_id") var id: Int? = -1 ) : Parcelable


data class DataItem(@Json(name = "img")
                    var img: String? = "",
                    @Json(name = "updated_at")
                    var updatedAt: String? = "",
                    @Json(name = "limited_products")
                    var limitedProducts: List<LimitedProductsItem>?,
                    @Json(name = "name")
                    var name: String = "",
                    @Json(name = "created_at")
                    var createdAt: String? = "",
                    @Json(name = "id")
                    var id: Int = 0)

@Parcelize
@Entity(tableName = "item_cart")
data class LimitedProductsItem(@Json(name = "images")
                               @Ignore var images: MutableList<ImagesItem>,
                               @Embedded var image: ImagesItem= images[0],
                               @Json(name = "section_id")
                               var sectionId: Int = -1,
                               @Json(name = "updated_at")
                               @Ignore var updatedAt: String = " ",
                               @Json(name = "sizes")
                               @Ignore var sizes: MutableList<SizesItem>,
                               @Embedded var size: SizesItem=SizesItem(),
                               @Json(name = "price")
                               var price: String = " ",
                               @Json(name = "name")
                               @ColumnInfo(name = "name")  var name: String = " ",
                               @Json(name = "description")
                               var description: String = " ",
                               @Json(name = "created_at")
                               @Ignore var createdAt: String = " ",
                               @Json(name = "id")
                               @PrimaryKey var id: Int = -1,
                               @Json(name = "order_id")
                               var orderId: Int? = 0) : Parcelable{
    constructor() : this(
        mutableListOf<ImagesItem>(),ImagesItem(),0,"",mutableListOf<SizesItem>(),SizesItem(),"","","",""
    ,0,0)
}


data class StoreModel(@Json(name = "data")
                      var data: List<DataItem>?,
                      @Json(name = "status")
                      var status: Status?)


