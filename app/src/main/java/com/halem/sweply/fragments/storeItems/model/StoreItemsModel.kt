package com.halem.sweply.fragments.storeItems.model


import com.halem.sweply.fragments.store.model.LimitedProductsItem
import com.squareup.moshi.Json

data class Status(@Json(name = "type")
                  val type: String = "",
                  @Json(name = "title")
                  val title: String = "")





data class StoreItemsModel(@Json(name = "data")
                           val data: Data?,
                           @Json(name = "status")
                           val status: Status?)







data class Data(@Json(name = "first_page_url")
                val firstPageUrl: String? = "",
                @Json(name = "path")
                val path: String? = "",
                @Json(name = "per_page")
                val perPage: String? = "",
                @Json(name = "total")
                val total: Int? = 0,
                @Json(name = "data")
                val data: List<LimitedProductsItem>?,
                @Json(name = "last_page")
                val lastPage: Int? = 0,
                @Json(name = "last_page_url")
                val lastPageUrl: String? = "",
                @Json(name = "next_page_url")
                val nextPageUrl: String? = null,
                @Json(name = "from")
                val from: Int? = 0,
                @Json(name = "to")
                val to: Int? = 0,
                @Json(name = "prev_page_url")
                val prevPageUrl: String? = null,
                @Json(name = "current_page")
                val currentPage: Int? = 0)


