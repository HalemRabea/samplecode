package com.halem.sweply.fragments.storeItems

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class StoreDetailsViewModelFactory(private val StoreId: Int,
                             private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(StoreItemsViewModel::class.java)) {
            return StoreItemsViewModel(StoreId, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}