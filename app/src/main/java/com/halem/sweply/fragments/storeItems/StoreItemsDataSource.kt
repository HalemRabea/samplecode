package com.halem.sweply.fragments.storeItems

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.halem.sweply.fragments.store.model.LimitedProductsItem
import com.halem.sweply.network.Api
import com.halem.sweply.utility.NetworkApiStatus
import kotlinx.coroutines.*

class StoreItemsDataSource(
    private val _StoreId: Int,
    private val _status: MutableLiveData<NetworkApiStatus>,
    private val _isLoading: MutableLiveData<Boolean>
) : PageKeyedDataSource<Int, LimitedProductsItem>() {
    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main )
    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, LimitedProductsItem>) {
//        val service = ApiServiceBuilder.buildService(ApiService::class.java)
//        val call = service.getUsers(params.key)
//        call.enqueue(object : Callback<UserResponse> {
//            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
//                if (response.isSuccessful) {
//                    val apiResponse = response.body()!!
//                    val responseItems = apiResponse.users
//                    val key = if (params.key > 1) params.key - 1 else 0
//                    responseItems?.let {
//                        callback.onResult(responseItems, key)
//                    }
//                }
//            }
//            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
//            }
//        })

//        coroutineScope.launch {
//            var getPropertiesDeferred = Api.retrofitService.getItemsSection(_StoreId,params.key)
//
//            try {
//
//                val listResult = getPropertiesDeferred.await()//.await()
////                _status.value = NetworkApiStatus.DONE
//                ("the before", listResult.data?.data?.size.toString())
////                _data.value = listResult.data?.data
//
//                val responseItems = listResult.data?.data
//                    val key = if (params.key > 1) params.key - 1 else 0
//                    responseItems?.let {
//                        callback.onResult(responseItems, key)
//                    }
//
//            } catch (e: Exception) {
////                _status.value = NetworkApiStatus.ERROR
////                _data.value = ArrayList()
//                ("the error", e.message.toString())
//                ("the errorE", e.toString())
//            }
//        }


    }
    @ExperimentalCoroutinesApi
    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, LimitedProductsItem>) {
        coroutineScope.launch {
            var getPropertiesDeferred = Api.retrofitService.getItemsSection(_StoreId,FIRST_PAGE)

            try {
                _status.value = NetworkApiStatus.LOADING
                val listResult = getPropertiesDeferred.await()//.await()
                _status.value = NetworkApiStatus.DONE
//                _data.value = listResult.data?.data

                val responseItems = listResult.data?.data
                responseItems?.let {
                    callback.onResult(responseItems, null, FIRST_PAGE + 1)
                    }
                _isLoading.value=false
            } catch (e: Exception) {
                _isLoading.value=false
                _status.value = NetworkApiStatus.ERROR
//                _data.value = ArrayList()
            }
        }
    }
    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, LimitedProductsItem>) {

        coroutineScope.launch {
            var getPropertiesDeferred = Api.retrofitService.getItemsSection(_StoreId,params.key)

            try {
                _status.value = NetworkApiStatus.LOADING
                val listResult = getPropertiesDeferred.await()//.await()
                _status.value = NetworkApiStatus.DONE
//                _data.value = listResult.data?.data

                val responseItems = listResult.data?.data
                val key = params.key + 1
                responseItems?.let {
                    callback.onResult(responseItems, key)
                }

            } catch (e: Exception) {
                _status.value = NetworkApiStatus.ERROR
//                _data.value = ArrayList()
            }
        }
    }
    companion object {
        const val PAGE_SIZE = 10
        const val FIRST_PAGE = 1
    }

}