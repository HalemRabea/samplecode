package com.halem.sweply.fragments.submitCart.model.modelSubmit


import com.squareup.moshi.Json

data class Status(@Json(name = "type")
                  val type: String = "",
                  @Json(name = "title")
                  val title: String = "")


data class ResponseSubmit(@Json(name = "status")
                          val status: Status)


