package com.halem.sweply.fragments.storeItems

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.halem.sweply.adapters.ItemsStoreAdapter
import com.halem.sweply.databinding.StoreItemsFragmentBinding
import kotlinx.android.synthetic.main.content_stor_base.*

class StoreItems : Fragment() {

    companion object {
        fun newInstance() = StoreItems()
    }

    private lateinit var viewModel: StoreItemsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        @Suppress("UNUSED_VARIABLE")
        val application = requireNotNull(activity).application
        val binding = StoreItemsFragmentBinding.inflate(inflater)
        val StoreId = StoreItemsArgs.fromBundle(arguments!!).storeId
        val viewModelFactory = StoreDetailsViewModelFactory(StoreId, application)


        // Allows Data Binding to Observe LiveData with the lifecycle of this Fragment
        binding.lifecycleOwner = this

        // Giving the binding access to the OverviewViewModel
        viewModel=ViewModelProviders.of(this, viewModelFactory).get(StoreItemsViewModel::class.java)
        binding.dataStores = viewModel

        binding.dataStores = viewModel

        binding.toolbar.title=StoreItemsArgs.fromBundle(arguments!!).title
        binding.toolbar.setNavigationOnClickListener {
            activity!!.onBackPressed()
        }

        binding.refresh.setOnRefreshListener {
            viewModel.itemsStorePagedList.value!!.dataSource.invalidate()
            viewModel.getDataServer()
        }

        binding.recyclerView.adapter =
            ItemsStoreAdapter(ItemsStoreAdapter.OnClickListener {
                viewModel.itemSectionsDetails(it)
            })

        viewModel.navigateToItemDetail.observe(this, Observer {
            if ( null != it ) {
                this.findNavController().navigate(StoreItemsDirections.actionStoreItemsToItemDetails(it))
                viewModel.displayItemDetailsComplete()
            }
        })

        viewModel.isLoading.observe(this , Observer {
        })

        return binding.root
    }

}
