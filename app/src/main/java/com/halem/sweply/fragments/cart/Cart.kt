package com.halem.sweply.fragments.cart

import android.graphics.Typeface
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

import com.halem.sweply.R
import com.halem.sweply.adapters.CartAdapter
import com.halem.sweply.dataBase.CartDatabase
import com.halem.sweply.databinding.CartFragmentBinding
import com.halem.sweply.fragments.store.model.LimitedProductsItem
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import androidx.recyclerview.widget.SimpleItemAnimator



class Cart : Fragment() {

    companion object {
        fun newInstance() = Cart()
    }

    private lateinit var viewModel: CartViewModel
    lateinit var binding: CartFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        @Suppress("UNUSED_VARIABLE")
        val application = requireNotNull(activity).application
        binding = CartFragmentBinding.inflate(inflater)
        val database = CartDatabase.getInstance(application).itemDao
        val viewModelFactory = CartViewModelFactory( application,database)

        binding.lifecycleOwner = this
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CartViewModel::class.java)
        binding.viewModel = viewModel

//        (mRecyclerView.getItemAnimator() as SimpleItemAnimator).supportsChangeAnimations = false
//        binding.recyclerView.itemAnimator!!.changeDuration=0
//        binding.recyclerView.itemAnimator!!.addDuration=0
        binding.recyclerView.adapter = CartAdapter(database)

        viewModel.cartData.observe(this , Observer {
            viewModel.getTotalItemCartBill()
        })

        val custom_Bold = Typeface.createFromAsset(activity!!.assets, "fonts/Mada-Bold.ttf")
        val custom_med = Typeface.createFromAsset(activity!!.assets, "fonts/Mada-Medium.ttf")
        val custom_reg = Typeface.createFromAsset(activity!!.assets, "fonts/Mada-Regular.ttf")

        binding.textView13.typeface=custom_reg
        binding.textView14.typeface=custom_reg

        return binding.root
    }




}
