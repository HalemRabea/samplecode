package com.halem.sweply.fragments.itemDetails

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.halem.sweply.MainActivity
import com.halem.sweply.dataBase.itemDao
import com.halem.sweply.fragments.store.model.LimitedProductsItem

class ItemDetailsViewModelFactory(
    private val Item: LimitedProductsItem,
    private val application: Application,
    private val dataSource: itemDao,
    private val mainActivity: MainActivity
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ItemDetailsViewModel::class.java)) {
            return ItemDetailsViewModel(Item, application,dataSource,mainActivity) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}