package com.halem.sweply.fragments.cart

import android.app.Application
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.halem.sweply.dataBase.itemDao
import com.halem.sweply.fragments.store.StoreFragmentDirections
import com.halem.sweply.fragments.store.model.LimitedProductsItem

class CartViewModel(
    val application: Application,
    dataSource: itemDao
) : ViewModel() {
    private val _TotalBill = MutableLiveData<String>("0")
    val TotalBill: LiveData<String>
        get() = _TotalBill
    val cartData = dataSource.getAllItems()

    fun getTotalItemCartBill() {
        var total = 0
        for (x in cartData.value!!) {
            total += (x.price.toInt() * (x.size.color!!.choosenQuanity.toInt()))
        }
        _TotalBill.value = total.toString()
    }

    fun onSubmitClick(view : View){
        if (cartData.value!!.isNotEmpty()){
            val items=arrayListOf<LimitedProductsItem>()
            items.addAll(cartData.value!!)
        view.findNavController().navigate(CartDirections.actionCartToSubmitCart22(items.toTypedArray()))
    }}
}
