package com.halem.sweply.fragments.submitCart

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.halem.sweply.dataBase.itemDao
import com.halem.sweply.fragments.store.model.LimitedProductsItem

class SubmitCartViewModelFactory(
    private val application: Application,
    val cartData: Array<LimitedProductsItem>,
    val database: itemDao
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SubmitCartViewModel::class.java)) {
            return SubmitCartViewModel(application,cartData,database) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}