package com.halem.sweply.fragments.submitCart.model.modelSendSubmit


import com.squareup.moshi.Json

data class SubmitModelSend(@Json(name = "address")
                           val address: String = "",
                           @Json(name = "quantity")
                           val quantity: List<String>?,
                           @Json(name = "size")
                           val size: List<String>?,
                           @Json(name = "color")
                           val color: List<String>?,
                           @Json(name = "phone")
                           val phone: String = "",
                           @Json(name = "district")
                           val district: String = "",
                           @Json(name = "name")
                           val name: String = "",
                           @Json(name = "products")
                           val products: List<String>?)


