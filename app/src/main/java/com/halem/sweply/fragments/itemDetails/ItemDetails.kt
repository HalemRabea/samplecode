package com.halem.sweply.fragments.itemDetails

import android.graphics.Color
import android.graphics.Typeface
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.*
import com.google.android.material.snackbar.Snackbar
import com.halem.sweply.MainActivity
import com.halem.sweply.R
import com.halem.sweply.dataBase.CartDatabase
import com.halem.sweply.databinding.ItemDetailsFragmentBinding
import com.halem.sweply.fragments.store.model.ColorsItem
import com.halem.sweply.fragments.store.model.SizesItem
import android.view.animation.AnimationUtils.loadAnimation
import android.R.string.no
import android.R.attr.name
import android.util.Log


class ItemDetails : Fragment() {

    var colors =ArrayList<ColorsItem>()
    companion object {
        fun newInstance() = ItemDetails()
    }

    private lateinit var viewModel: ItemDetailsViewModel
    lateinit var colorAdapter: BaseAdapter
    lateinit var binding: ItemDetailsFragmentBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        @Suppress("UNUSED_VARIABLE")
        val application = requireNotNull(activity).application
        binding = ItemDetailsFragmentBinding.inflate(inflater)
        val itemDetail = ItemDetailsArgs.fromBundle(arguments!!).itemDetail
        val dataSource = CartDatabase.getInstance(application).itemDao
        val viewModelFactory = ItemDetailsViewModelFactory(itemDetail, application,dataSource,activity as MainActivity)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ItemDetailsViewModel::class.java)

        binding.lifecycleOwner = this

        binding.dataItem = viewModel
        binding.slider=binding.imageSlider

        binding.toolbar.setNavigationOnClickListener {
            activity!!.onBackPressed()
        }

        if (viewModel.item.value!!.sizes.size>0) {
            if (viewModel.item.value!!.sizes[0].id != 0)
                viewModel.item.value!!.sizes.add(
                    0,
                    SizesItem(getString(R.string.chooseSize), "", 0, "", 0, mutableListOf<ColorsItem>(),ColorsItem())
                )
        }else
            viewModel.item.value!!.sizes.add(0, SizesItem(getString(R.string.chooseSize), "", 0, "", 0, mutableListOf<ColorsItem>(),ColorsItem()))
        val sizeAdapter = object : ArrayAdapter<SizesItem>(
            activity!!,
            R.layout.support_simple_spinner_dropdown_item,
            viewModel.item.value!!.sizes
        ) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }

            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val view = super.getDropDownView(position, convertView, parent)
                val tv = view as TextView
                val custom_med = Typeface.createFromAsset(view.context.assets, "fonts/Mada-Medium.ttf")
                tv.typeface=custom_med

                if (position == 0) {
                    tv.setTextColor(Color.parseColor("#CF9E29"))
                    tv.text = getString(R.string.chooseSize)
                } else {
                    tv.setTextColor(Color.BLACK)
                    tv.text = viewModel.item.value!!.sizes[position].size
                }

                return view
            }

        }

        with(binding.sizeSpinner)
        {
            adapter = sizeAdapter
            prompt = getString(R.string.chooseSize)
            gravity = Gravity.START
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    val item = parent!!.selectedItem as SizesItem
                    (view as TextView).text = item.size
                    view.gravity=Gravity.START
                    val custom_med = Typeface.createFromAsset(view.context.assets, "fonts/Mada-Medium.ttf")
                    view.typeface=custom_med
                    if (position!=0) {
                        colors.clear()
                        colors.add(0, ColorsItem("","","",0,getString(R.string.choosecolor),0,"","",0))
                        viewModel.sizesId.value=item.id
                        initalColorSpinner(item.colors)
                        viewModel.Quatntiy.value="0"
                    }else (view ).setTextColor(Color.parseColor("#CF9E29"))
                }
            }
        }



        colors.add(0, ColorsItem("","","",0,getString(R.string.choosecolor),0,"","",0))
        colorAdapter=object : ArrayAdapter<ColorsItem>(activity!!, R.layout.support_simple_spinner_dropdown_item, colors) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }
            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val view = super.getDropDownView(position, convertView, parent)
                val tv = view as TextView
                val custom_med = Typeface.createFromAsset(view.context.assets, "fonts/Mada-Medium.ttf")
                tv.typeface=custom_med
                if (position == 0) {
                    tv.setTextColor(Color.parseColor("#CF9E29"))
                    tv.text = getString(R.string.choosecolor)
                } else {
                    tv.setTextColor(Color.BLACK)
                    tv.text = colors[position].name
                }

                return view
            }

        }

        binding.colorSpinner.setOnTouchListener { v, event ->
            if(colors.size==1){
                val animShake = loadAnimation(context, R.anim.shake)
                binding.sizeSpinner.startAnimation(animShake)
                Snackbar.make(
                    binding.root,
                    application.getString(R.string.chooseSizeFirst),
                    Snackbar.LENGTH_SHORT
                ).setAction("Action", null).show()
            }
            return@setOnTouchListener false
        }


        with(binding.colorSpinner)
        {
            adapter = colorAdapter
            prompt = getString(R.string.choosecolor)
            gravity = Gravity.START
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    val item = parent!!.selectedItem as ColorsItem
                    (view as TextView).text = item.name
                    view.gravity=Gravity.START
                    val custom_med = Typeface.createFromAsset(view.context.assets, "fonts/Mada-Medium.ttf")
                    view.typeface=custom_med
                    viewModel.ColorsId.value=item.id
                    if (position==0)
                        (view ).setTextColor(Color.parseColor("#CF9E29"))
                    else viewModel.limitQuatntiy.value= Integer.parseInt(item.quantity)
                }
            }
        }
        return binding.root
    }

    private fun initalColorSpinner(color: MutableList<ColorsItem>) {
        colors.addAll(color)
        colorAdapter=object : ArrayAdapter<ColorsItem>(activity!!, R.layout.support_simple_spinner_dropdown_item, colors) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }
            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val view = super.getDropDownView(position, convertView, parent)
                val tv = view as TextView
                val custom_med = Typeface.createFromAsset(view.context.assets, "fonts/Mada-Medium.ttf")
                tv.typeface=custom_med
                if (position == 0) {
                    tv.setTextColor(Color.parseColor("#CF9E29"))
                    tv.text = getString(R.string.choosecolor)
                } else {
                    tv.setTextColor(Color.BLACK)
                    tv.text = colors[position].name
                }

                return view
            }
        }
        with(binding.colorSpinner)
        {
            adapter = colorAdapter
            prompt = getString(R.string.choosecolor)
            gravity = Gravity.START
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    val item = parent!!.selectedItem as ColorsItem
                    (view as TextView).text = item.name
                    view.gravity=Gravity.START
                    val custom_med = Typeface.createFromAsset(view.context.assets, "fonts/Mada-Medium.ttf")
                    view.typeface=custom_med
                    viewModel.Quatntiy.value="0"
                    viewModel.ColorsId.value=item.id
                    if (position==0)
                        (view ).setTextColor(Color.parseColor("#CF9E29"))
                    else viewModel.limitQuatntiy.value= Integer.parseInt(item.quantity)
                }
            }
        }
    }


}
