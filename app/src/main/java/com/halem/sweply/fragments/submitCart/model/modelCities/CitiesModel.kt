package com.halem.sweply.fragments.submitCart.model.modelCities


import com.squareup.moshi.Json

data class Status(@Json(name = "type")
                  val type: String? = "",
                  @Json(name = "title")
                  val title: String? = "")


data class DataItem(@Json(name = "updated_at")
                    val updatedAt: String? = "",
                    @Json(name = "name")
                    val name: String? = "",
                    @Json(name = "districts")
                    val districts: List<DistrictsItem>?,
                    @Json(name = "created_at")
                    val createdAt: String? = "",
                    @Json(name = "id")
                    val id: Int? = 0)


data class CitesModel(@Json(name = "data")
                      val data: List<DataItem>?,
                      @Json(name = "status")
                      val status: Status?)


data class DistrictsItem(@Json(name = "shipping")
                         val shipping: String? = "",
                         @Json(name = "updated_at")
                         val updatedAt: String? = "",
                         @Json(name = "name")
                         val name: String? = "",
                         @Json(name = "created_at")
                         val createdAt: String? = "",
                         @Json(name = "id")
                         val id: Int? = 0,
                         @Json(name = "city_id")
                         val cityId: Int? = 0)


