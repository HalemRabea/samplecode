package com.halem.sweply.fragments.store

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.halem.sweply.utility.NetworkApiStatus
import com.halem.sweply.fragments.store.model.DataItem
import com.halem.sweply.network.Api
import kotlinx.coroutines.*

class StoreViewModel : ViewModel() {
    private val _status = MutableLiveData<NetworkApiStatus>()
    val status: LiveData<NetworkApiStatus>
        get() = _status

    private val _isLoading = MutableLiveData<Boolean>(false)
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    private val _data = MutableLiveData<List<DataItem>?>()
    val arrayData: LiveData<List<DataItem>?>
        get() = _data

    private val _navigateToSelectedStore = MutableLiveData<DataItem>()

    val navigateToSelectedProperty: LiveData<DataItem>
        get() = _navigateToSelectedStore

    //Add variables for a co-routine Job and a Co-routineScope using the Main Dispatcher:
    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main )

    init {
        getDataServer()
    }

    fun displaySectionsDetails(section: DataItem) {
        _navigateToSelectedStore.value = section
    }

    fun displayPropertyDetailsComplete() {
        _navigateToSelectedStore.value = null
    }

     fun getDataServer() {
        coroutineScope.launch {
            var getPropertiesDeferred = Api.retrofitService.getSections()

            try {
                _status.value = NetworkApiStatus.LOADING

                val listResult = getPropertiesDeferred.await()//.await()
                _status.value = NetworkApiStatus.DONE
                _data.value = listResult.data
                _isLoading.value=false
            } catch (e: Exception) {
                _isLoading.value=false
                _status.value = NetworkApiStatus.ERROR
                _data.value = ArrayList()
            }
//        _status.value = NetworkApiStatus.LOADING
//
//        val service = RetrofitFactory.makeRetrofitService()
//        CoroutineScope(Dispatchers.IO).launch {
//            val response = service.getSections()
//            try {
//                withContext(Dispatchers.Main) {
//                    if (response.isSuccessful) {
////                        response.body()?.let { initRecyclerView(it) }
//
//                        val listResult = response.body()?.data
//                        _status.value = NetworkApiStatus.DONE
//                        ("the error",response.body().toString())
//                        _data.value = listResult
//                    } else {
//                        _status.value = NetworkApiStatus.ERROR
//                        _data.value = ArrayList()
//                    }
//                }
//            } catch (e: Exception) {
//                Log.e("REQUEST", "Exception ${e.message}")
//            } catch (e: Throwable) {
//                Log.e("REQUEST", "Ooops: Something else went wrong")
//            }
//        }
        }

    }
}
