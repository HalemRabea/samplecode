package com.halem.sweply.fragments.store

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.halem.sweply.adapters.StoreAdapter

import com.halem.sweply.R
import com.halem.sweply.databinding.StoreFragmentBinding
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController


class StoreFragment : Fragment() {

    companion object {
        fun newInstance() = StoreFragment()
    }

    private val viewModel: StoreViewModel by lazy {
        ViewModelProviders.of(this).get(StoreViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        val binding = StoreFragmentBinding.inflate(inflater)

        // Allows Data Binding to Observe LiveData with the lifecycle of this Fragment
        binding.lifecycleOwner = this

        // Giving the binding access to the OverviewViewModel
        binding.datas = viewModel
        binding.refresh.setOnRefreshListener {
            viewModel.getDataServer()
        }
        binding.dataStores.adapter =
            StoreAdapter(StoreAdapter.OnClickListener {
                viewModel.displaySectionsDetails(it)
            })

        viewModel.navigateToSelectedProperty.observe(this, Observer {
            if ( it != null ) {
                this.findNavController().navigate(StoreFragmentDirections.actionStoreFragmentToStoreItems(it.id,it.name))
                viewModel.displayPropertyDetailsComplete()
            }
        })

        return binding.root
    }

}
