package com.halem.sweply.fragments.storeItems

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.halem.sweply.fragments.store.model.LimitedProductsItem
import com.halem.sweply.utility.NetworkApiStatus

class StoreItemsDataSourceFactory(
    private val sotreId: Int,
    private val _status: MutableLiveData<NetworkApiStatus>,
    private val _isLoading: MutableLiveData<Boolean>
) : DataSource.Factory<Int, LimitedProductsItem>() {
    var userLiveDataSource = MutableLiveData<StoreItemsDataSource>()
    override fun create(): DataSource<Int, LimitedProductsItem> {
        val userDataSource = StoreItemsDataSource(sotreId,_status,_isLoading)
        userLiveDataSource.postValue(userDataSource)
        return userDataSource
    }
}