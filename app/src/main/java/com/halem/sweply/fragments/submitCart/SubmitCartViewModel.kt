package com.halem.sweply.fragments.submitCart

import android.app.Application
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.material.snackbar.Snackbar
import com.halem.sweply.R
import com.halem.sweply.dataBase.itemDao
import com.halem.sweply.fragments.store.model.LimitedProductsItem
import com.halem.sweply.fragments.submitCart.model.modelCities.DataItem
import com.halem.sweply.fragments.submitCart.model.modelSendSubmit.SubmitModelSend
import com.halem.sweply.network.Api
import kotlinx.coroutines.*

class SubmitCartViewModel(
    val application: Application,
    val dataSource: Array<LimitedProductsItem>,
    val database: itemDao
) : ViewModel() {
    //Add variables for a co-routine Job and a Co-routineScope using the Main Dispatcher:
    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)
//    val cartData = dataSource.getAllItems()

//    val cartItems=ArrayList<LimitedProductsItem>()

    private val _DataCites = MutableLiveData<List<DataItem>?>()
    val DataCites: LiveData<List<DataItem>?>
        get() = _DataCites

    var name= MutableLiveData<String>("")
    var phone= MutableLiveData<String>("")
    var address= MutableLiveData<String>("")

    var shipping=MutableLiveData<String>("0")
    var DistrictId=MutableLiveData<Int>(0)

    var sendSuccess=MutableLiveData<Boolean>(false)


    init {
        getCitiesServer()
//        cartItems.addAll(cartData.value!!)
    }

    fun onclickSubmit(view : View){
        if(name.value!!.isEmpty()){
            showError(application.getString(R.string.pleaseAddName),view)
        return
        }
    if(phone.value!!.isEmpty()){
            showError(application.getString(R.string.pleaseAddPhone),view)
        return
        }
    if(address.value!!.isEmpty()){
            showError(application.getString(R.string.pleaseAddAddress),view)
        return
        }
    if(DistrictId.value==0){
            showError(application.getString(R.string.pleaseAdddistrict),view)
        return
        }

        getDataCart()

    }

    private fun getDataCart() {
        val products=ArrayList<String>()
        val size=ArrayList<String>()
        val color=ArrayList<String>()
        val quantity=ArrayList<String>()

        for(item in dataSource){
            products.add(item.id.toString())
            size.add(item.size.id.toString())
            color.add(item.size.color!!.id.toString())
            quantity.add(item.size.color!!.choosenQuanity)
        }



        coroutineScope.launch {
            var submitCart = Api.retrofitService.submitOrder(SubmitModelSend(address.value!!,quantity,size,color,phone.value!!,DistrictId.value!!.toString(),name.value!!,products))

            try {

                val listResult = submitCart.await()//.await()
                if (listResult.status.type == "0"){
                    Toast.makeText(application,listResult.status.title,Toast.LENGTH_LONG).show()
                }else{
//                    dataSource.clearAll()
                    Toast.makeText(application,application.getString(R.string.submitSuccessful),Toast.LENGTH_LONG).show()
                    deleteDataBase()
                }

            } catch (e: Exception) {
//                _DataCites.value = ArrayList()
            }
        }

    }

    private fun deleteDataBase() {

         var viewModelJob = Job()
         val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
        uiScope.launch {
            withContext(Dispatchers.IO) {
                database.clearAll()
            }
        }
        sendSuccess.value=true
    }

    private fun getCitiesServer() {
        coroutineScope.launch {
            var getPropertiesDeferred = Api.retrofitService.getCites()

            try {

                val listResult = getPropertiesDeferred.await()//.await()
//                ("the error", listResult.data?.size.toString())
                _DataCites.value = listResult.data

            } catch (e: Exception) {
                _DataCites.value = ArrayList()
            }
        }
    }

    fun showError(s:String,view:View){
        Snackbar.make(
            view,
            s,
            Snackbar.LENGTH_LONG
        ).setAction("Action", null).show()
    }
}
