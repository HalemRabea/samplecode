package com.halem.sweply.fragments.itemDetails

import android.app.Application
import android.view.View
import android.widget.Spinner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.material.snackbar.Snackbar
import com.halem.sweply.R
import com.halem.sweply.fragments.store.model.LimitedProductsItem
import android.widget.TextView
import com.halem.sweply.MainActivity
import com.halem.sweply.dataBase.itemDao
import com.halem.sweply.fragments.store.model.ColorsItem
import com.halem.sweply.fragments.store.model.SizesItem
import kotlinx.coroutines.*


class ItemDetailsViewModel(
    val itemD: LimitedProductsItem,
    val application: Application,
    val database: itemDao,
    val mainActivity: MainActivity
) : ViewModel() {

    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private val _item = MutableLiveData<LimitedProductsItem>()
    val item: LiveData<LimitedProductsItem>
        get() = _item

    var Quatntiy = MutableLiveData<String>()
    var limitQuatntiy = MutableLiveData<Int>()
    var sizesId = MutableLiveData<Int>(0)
    var ColorsId = MutableLiveData<Int>(0)

    init {
        _item.value = itemD
        Quatntiy.value = "0"
        limitQuatntiy.value = 0
    }

    fun onClickPlus(view: View) {
        if (sizesId.value != 0 && ColorsId.value != 0) {
            if (limitQuatntiy.value!! > 0) {
                if (Integer.parseInt(Quatntiy.value.toString()) < limitQuatntiy.value!!) {
                    Quatntiy.value = (Quatntiy.value!!.toInt() + 1).toString()
                } else {
                    Snackbar.make(
                        view,
                        application.getString(R.string.thereIsNoMoreEngouhQuantity),
                        Snackbar.LENGTH_SHORT
                    ).setAction("Action", null).show()
                }
            } else {
                Snackbar.make(
                    view,
                    application.getString(R.string.thereIsNoMoreEngouhQuantity),
                    Snackbar.LENGTH_LONG
                ).setAction("Action", null).show()
            }
        } else {
            Snackbar.make(
                view,
                application.getString(R.string.chooseSizeAndColor),
                Snackbar.LENGTH_LONG
            ).setAction("Action", null).show()
        }
    }

    fun onClickMinus(view: View) {
        if (Integer.parseInt(Quatntiy.value.toString()) > 1) {
            Quatntiy.value = (Quatntiy.value!!.toInt() - 1).toString()
        } else Snackbar.make(
            view,
            application.getString(R.string.cantTakeQuantityLessThan) + " " + Quatntiy.value,
            Snackbar.LENGTH_LONG
        ).setAction("Action", null).show()

    }

    fun SubmitCart(view: View, sizes: Spinner, colors: Spinner, quantityTv: TextView,sliderContainer :View) {
        if (sizes.selectedItemPosition == 0) {
            Snackbar.make(
                view,
                application.getString(R.string.pleaseChooseSize),
                Snackbar.LENGTH_LONG
            ).setAction("Action", null).show()
            return
        } else if (colors.selectedItemPosition == 0) {
            Snackbar.make(
                view,
                application.getString(R.string.pleaseChooseColor),
                Snackbar.LENGTH_LONG
            ).setAction("Action", null).show()
            return
        } else if (Integer.parseInt(quantityTv.text.toString()) !in 1..limitQuatntiy.value!!) {
            Snackbar.make(
                view,
                application.getString(R.string.thereIsNoMoreEngouhQuantity),
                Snackbar.LENGTH_LONG
            ).setAction("Action", null).show()
            return
        } else {
//            Toast.makeText(application, "submit", Toast.LENGTH_LONG).show()
            val size=sizes.selectedItem as SizesItem
            val color =colors.selectedItem as ColorsItem
            color.quantity=(Integer.parseInt(color.quantity)-Integer.parseInt(quantityTv.text.toString())).toString()
            color.choosenQuanity=quantityTv.text.toString()
            size.color=color
            _item.value!!.size=size

            limitQuatntiy.value=Integer.parseInt(color.quantity)

            uiScope.launch {
                withContext(Dispatchers.IO) {
                    val exist=database.get(_item.value!!.id)
                    if (exist==null)
                    database.insert(_item.value!!)
                    else{
                        _item.value!!.size.color!!.quantity=(Integer.parseInt(color.quantity)-Integer.parseInt(quantityTv.text.toString())).toString()
                        _item.value!!.size.color!!.choosenQuanity=(Integer.parseInt(quantityTv.text.toString())
                                +Integer.parseInt(exist.size.color!!.choosenQuanity)).toString()
                        database.update(_item.value!!)
//                        limitQuatntiy.value=Integer.parseInt(_item.value!!.size.color!!.quantity)
                    }
                }
            }


//            Snackbar.make(
//                view,
//                application.getString(R.string.addedToCartSuccess),
//                Snackbar.LENGTH_LONG
//            ).setAction("Action", null).show()
            //call method in main activity to animat
            mainActivity.takeScreenShot(sliderContainer)
        }
    }
}





