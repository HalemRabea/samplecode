package com.halem.sweply.fragments.about

import android.content.Intent
import android.net.Uri
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.halem.sweply.R
import com.halem.sweply.databinding.AboutFragmentBinding
import android.content.pm.PackageManager
import android.content.pm.ApplicationInfo
import android.content.ActivityNotFoundException
import android.graphics.Typeface


class About : Fragment() {

    companion object {
        fun newInstance() = About()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = AboutFragmentBinding.inflate(inflater)
        // Allows Data Binding to Observe LiveData with the lifecycle of this Fragment
        binding.lifecycleOwner = this

        binding.facebook.setOnClickListener {
            val intent=newFacebookIntent(activity!!.packageManager,"https://www.facebook.com/sweply-1879114585471588/")
            startActivity(intent)
        }

        binding.instagram.setOnClickListener {

            val uri = Uri.parse("https://www.instagram.com/swep.ly/")
            val likeIng = Intent(Intent.ACTION_VIEW, uri)

            likeIng.setPackage("com.instagram.android")

            try {
                startActivity(likeIng)
            } catch (e: ActivityNotFoundException) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://www.instagram.com/swep.ly/")
                    )
                )
            }


        }
        binding.snapchat.setOnClickListener {

            try {
                val intent =
                    Intent(Intent.ACTION_VIEW, Uri.parse("https://www.snapchat.com/add/swep_libya"))
                intent.setPackage("com.snapchat.android")
                startActivity(intent)
            } catch (e: Exception) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://www.snapchat.com/add/swep_libya")
                    )
                )
            }

        }
        binding.imageView6.setOnClickListener {
            //whatsapp whatsapp://send?phone=218916615261
            val number=218916615261
            val url = "https://api.whatsapp.com/send?phone=$number"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }
        binding.textView7.setOnClickListener {
//            email
//            val mIntent = Intent(Intent.ACTION_SENDTO)
////            mIntent.data = Uri.parse("mailto:")
//            mIntent.type = "message/rfc822"
//            mIntent.putExtra(Intent.EXTRA_EMAIL, "SwepLibya@gmail.com")
//            try {
//                //start email intent
//                startActivity(Intent.createChooser(mIntent, "Send Email"))
//            }
//            catch (e: Exception){
//                //if any thing goes wrong for example no email client application or any exception
//                //get and show exception message
//                Toast.makeText(activity, e.message, Toast.LENGTH_LONG).show()
//            }

            val intent = Intent(Intent.ACTION_SENDTO)
            intent.data = Uri.parse("mailto:") // only email apps should handle this
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf<String>("SwepLibya@gmail.com"))
            intent.putExtra(Intent.EXTRA_SUBJECT,"Feedback")
            try {
                //start email intent
                startActivity(intent)
            }
            catch (e: Exception){
                //if any thing goes wrong for example no email client application or any exception
                //get and show exception message
                Toast.makeText(activity, e.message, Toast.LENGTH_LONG).show()
            }
        }
//font
        val custom_Bold = Typeface.createFromAsset(activity!!.assets, "fonts/Mada-Bold.ttf")
        val custom_med = Typeface.createFromAsset(activity!!.assets, "fonts/Mada-Medium.ttf")
        val custom_reg = Typeface.createFromAsset(activity!!.assets, "fonts/Mada-Regular.ttf")
        binding.textView.typeface=custom_Bold
        binding.textView4.typeface=custom_reg
        binding.textView7.typeface=custom_reg
        binding.textView6.typeface=custom_med

        return binding.root
    }

    fun newFacebookIntent(pm: PackageManager, url: String): Intent {
        var uri = Uri.parse(url)
        try {
            val applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0)
            if (applicationInfo.enabled) {
                // http://stackoverflow.com/a/24547437/1048340
                uri = Uri.parse("fb://facewebmodal/f?href=$url")
            }
        } catch (ignored: PackageManager.NameNotFoundException) {
        }

        return Intent(Intent.ACTION_VIEW, uri)
    }
}