package com.halem.sweply.fragments.submitCart

import android.graphics.Color
import android.graphics.Typeface
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController

import com.halem.sweply.R
import com.halem.sweply.dataBase.CartDatabase
import com.halem.sweply.databinding.CaustomDialogLayoutBinding
import com.halem.sweply.fragments.submitCart.model.modelCities.DataItem
import com.halem.sweply.fragments.submitCart.model.modelCities.DistrictsItem

class SubmitCart : Fragment() {

    companion object {
        fun newInstance() = SubmitCart()
    }

    private lateinit var viewModel: SubmitCartViewModel
    lateinit var binding: CaustomDialogLayoutBinding

    var Cities= mutableListOf<DataItem>()

    var District= mutableListOf<DistrictsItem>()

   lateinit var districtAdapter: BaseAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        @Suppress("UNUSED_VARIABLE")
        val application = requireNotNull(activity).application
        binding = CaustomDialogLayoutBinding.inflate(inflater)
        val database = CartDatabase.getInstance(application).itemDao
        val viewModelFactory = SubmitCartViewModelFactory( application,SubmitCartArgs.fromBundle(arguments!!).cartData,database)

        binding.lifecycleOwner = this
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SubmitCartViewModel::class.java)
        binding.viewModel = viewModel

        binding.toolbar.setNavigationOnClickListener {
            activity!!.onBackPressed()
        }

        initSpinners()

        viewModel.DataCites.observe(this, Observer {
            if (it!!.isNotEmpty()){
                Cities.addAll(it)
            }
        })

        viewModel.shipping.observe(this, Observer {
        })

        viewModel.DistrictId.observe(this , Observer {
        })

        viewModel.sendSuccess.observe(this, Observer {
            if (it){
                findNavController().popBackStack()
                viewModel.sendSuccess.value=false
            }
        })

        return  binding.root
    }

    private fun initSpinners() {
//        if (Cities.size>0) {
//            if (Cities[0].id != 0){
                District.add(0, DistrictsItem("0","",getString(R.string.chooseDistrict),"",0,0))
                Cities.add(0, DataItem("",getString(R.string.city), District,"",0)
                )
//            }
//        }
        val citiesAdapter = object : ArrayAdapter<DataItem>(
            activity!!,
            R.layout.support_simple_spinner_dropdown_item,
            Cities
        ) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }

            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val view = super.getDropDownView(position, convertView, parent)
                val tv = view as TextView
                val custom_med = Typeface.createFromAsset(view.context.assets, "fonts/Mada-Medium.ttf")
                tv.typeface=custom_med
                if (position == 0) {
                    tv.setTextColor(Color.parseColor("#CF9E29"))
                    tv.text = getString(R.string.city)
                } else {
                    tv.setTextColor(Color.BLACK)
                    tv.text =Cities[position].name
                }

                return view
            }

        }

        with(binding.CitesSpinner)
        {
            adapter = citiesAdapter
            prompt = getString(com.halem.sweply.R.string.city)
            gravity = android.view.Gravity.START
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    val item = parent!!.selectedItem as DataItem
                    (view as TextView).text = item.name
                    view.gravity=Gravity.START
                    val custom_med = Typeface.createFromAsset(view.context.assets, "fonts/Mada-Medium.ttf")
                    view.typeface=custom_med
                    if (position!=0) {
                        District.clear()
                        District.add(0, DistrictsItem("0","",getString(R.string.chooseDistrict),"",0,0))
                        initalDistrictSpinner(item.districts!!)
                    }else (view ).setTextColor(android.graphics.Color.parseColor("#CF9E29"))
                }
            }
        }



//        colors.add(0, ColorsItem("","","",0,getString(R.string.choosecolor),0,"","",0))
        districtAdapter=object : ArrayAdapter<DistrictsItem>(activity!!, R.layout.support_simple_spinner_dropdown_item, District) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }
            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val view = super.getDropDownView(position, convertView, parent)
                val tv = view as TextView
                val custom_med = Typeface.createFromAsset(view.context.assets, "fonts/Mada-Medium.ttf")
                tv.typeface=custom_med
                if (position == 0) {
                    tv.setTextColor(Color.parseColor("#CF9E29"))
                    tv.text = getString(R.string.chooseDistrict)
                } else {
                    tv.setTextColor(Color.BLACK)
                    tv.text = District[position].name
                }

                return view
            }

        }

        with(binding.districtSpinner)
        {
            adapter = districtAdapter
            prompt = getString(com.halem.sweply.R.string.chooseDistrict)
            gravity = android.view.Gravity.START
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    val item = parent!!.selectedItem as DistrictsItem
                    (view as TextView).text = item.name
                    view.gravity=Gravity.START
                    val custom_med = Typeface.createFromAsset(view.context.assets, "fonts/Mada-Medium.ttf")
                    view.typeface=custom_med
                    if (position==0)
                        (view ).setTextColor(android.graphics.Color.parseColor("#CF9E29"))
                    else{viewModel.shipping.value=item.shipping
                        viewModel.DistrictId.value=item.id
                    }
                }
            }
        }
    }


    private fun initalDistrictSpinner(districts: List<DistrictsItem>) {
        District.addAll(districts)
        districtAdapter=object : ArrayAdapter<DistrictsItem>(activity!!, R.layout.support_simple_spinner_dropdown_item, District) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }
            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val view = super.getDropDownView(position, convertView, parent)
                val tv = view as TextView
                val custom_med = Typeface.createFromAsset(view.context.assets, "fonts/Mada-Medium.ttf")
                tv.typeface=custom_med
                if (position == 0) {
                    tv.setTextColor(Color.parseColor("#CF9E29"))
                    tv.text = getString(R.string.chooseDistrict)
                } else {
                    tv.setTextColor(Color.BLACK)
                    tv.text = District[position].name
                }

                return view
            }

        }

        with(binding.districtSpinner)
        {
            adapter = districtAdapter
            prompt = getString(com.halem.sweply.R.string.chooseDistrict)
            gravity = android.view.Gravity.START
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    val item = parent!!.selectedItem as DistrictsItem
                    (view as TextView).text = item.name
                    view.gravity= Gravity.START
                    val custom_med = Typeface.createFromAsset(view.context.assets, "fonts/Mada-Medium.ttf")
                    view.typeface=custom_med

                    if (position==0)
                        (view ).setTextColor(android.graphics.Color.parseColor("#CF9E29"))
                    else{viewModel.shipping.value=item.shipping
                        viewModel.DistrictId.value=item.id
                    }
                }
            }
        }
    }

}
