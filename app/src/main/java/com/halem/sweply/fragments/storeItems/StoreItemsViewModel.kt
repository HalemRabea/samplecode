package com.halem.sweply.fragments.storeItems

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
//import androidx.paging.LivePagedListBuilder
//import androidx.paging.PagedList
import com.halem.sweply.utility.NetworkApiStatus
import com.halem.sweply.fragments.store.model.LimitedProductsItem

class StoreItemsViewModel (private val StoreId: Int, app: Application) : AndroidViewModel(app)  {
//    private val _StoreId:Int = StoreId

    private val _isLoading = MutableLiveData<Boolean>(false)
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    private val _status = MutableLiveData<NetworkApiStatus>()
    val status: LiveData<NetworkApiStatus>
        get() = _status

//    private val _data = MutableLiveData<List<LimitedProductsItem>?>()
//    val arrayData: LiveData<List<LimitedProductsItem>?>
//        get() = _data

    private val _navigateToItemDetail = MutableLiveData<LimitedProductsItem>()
    val navigateToItemDetail: LiveData<LimitedProductsItem>
        get() = _navigateToItemDetail

    fun itemSectionsDetails(section: LimitedProductsItem) {
        _navigateToItemDetail.value = section
    }

    fun displayItemDetailsComplete() {
        _navigateToItemDetail.value = null
    }

    lateinit var itemsStorePagedList: LiveData<PagedList<LimitedProductsItem>>
    private lateinit var liveDataSource: LiveData<StoreItemsDataSource>


    //Add variables for a co-routine Job and a Co-routineScope using the Main Dispatcher:
//    private var viewModelJob = Job()
//    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main )

    init {
        getDataServer()
    }


     fun getDataServer() {
        val itemDataSourceFactory = StoreItemsDataSourceFactory(StoreId,_status,_isLoading)
        liveDataSource = itemDataSourceFactory.userLiveDataSource
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(StoreItemsDataSource.PAGE_SIZE)
            .build()
        itemsStorePagedList = LivePagedListBuilder(itemDataSourceFactory, config)
            .build()
    }

}
