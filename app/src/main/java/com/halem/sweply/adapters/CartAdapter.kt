package com.halem.sweply.adapters


import android.content.Context
import android.graphics.Typeface
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.halem.sweply.R
import com.halem.sweply.dataBase.itemDao
import com.halem.sweply.databinding.CartItemLayoutBinding
import com.halem.sweply.fragments.cart.CartViewModelFactory
import com.halem.sweply.fragments.store.model.LimitedProductsItem
import kotlinx.coroutines.*


class CartAdapter(val dataSource: itemDao) : ListAdapter<LimitedProductsItem, CartAdapter.StoreViewHolder>(
    DiffCallback
) {
//    lateinit var binding:StoreLayoutBinding
private var viewModelJob = Job()

    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    companion object DiffCallback : DiffUtil.ItemCallback<LimitedProductsItem>() {
        override fun areItemsTheSame(oldItem: LimitedProductsItem, newItem: LimitedProductsItem): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: LimitedProductsItem, newItem: LimitedProductsItem): Boolean {
            return oldItem.id == newItem.id
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreViewHolder {
        return StoreViewHolder(
             CartItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context), parent, false),parent.context)
    }

    override fun onBindViewHolder(holder: StoreViewHolder, position: Int) {
        val marsProperty = getItem(position)
        holder.bind(marsProperty)

        holder.binding.plus.setOnClickListener {
//            if (marsProperty.size.color!!.quantity.toInt() > 0) {
                marsProperty.size.color!!.choosenQuanity = (marsProperty.size.color!!.choosenQuanity.toInt()+ 1).toString()
//                marsProperty.size.color!!.quantity=(marsProperty.size.color!!.quantity.toInt()-1).toString()
                notifyDataSetChanged()
                uiScope.launch {
                    withContext(Dispatchers.IO) {
                        dataSource.update(marsProperty)
                    }
                }
//            } else {
//                Snackbar.make(
//                    holder.binding.root,
//                    holder.context.getString(R.string.thereIsNoMoreEngouhQuantity),
//                    Snackbar.LENGTH_LONG
//                ).setAction("Action", null).show()
//            }
        }


        holder.binding.minus.setOnClickListener {
            if (marsProperty.size.color!!.choosenQuanity.toInt() > 1) {
                marsProperty.size.color!!.choosenQuanity= (marsProperty.size.color!!.choosenQuanity.toInt() - 1).toString()
//                marsProperty.size.color!!.quantity=(marsProperty.size.color!!.quantity.toInt()+1).toString()
                notifyDataSetChanged()
                uiScope.launch {
                    withContext(Dispatchers.IO) {
                        dataSource.update(marsProperty)
                    }
                }
            } else Snackbar.make(
                holder.binding.root,
                holder.context.getString(R.string.cantTakeQuantityLessThan) + " " + marsProperty.size.color!!.choosenQuanity,
                Snackbar.LENGTH_LONG
            ).setAction("Action", null).show()
        }

        holder.binding.delete.setOnClickListener {
            uiScope.launch {
                withContext(Dispatchers.IO) {
                    dataSource.delete(marsProperty)
                }
            }
        }
    }


    class StoreViewHolder( var binding: CartItemLayoutBinding, val context: Context):
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            storeItem: LimitedProductsItem
        ) {
//            viewModel._CartData.value=storeItem
            binding.dataItem = storeItem
            binding.executePendingBindings()

            val custom_Bold = Typeface.createFromAsset(context.assets, "fonts/Mada-Bold.ttf")
            val custom_med = Typeface.createFromAsset(context.assets, "fonts/Mada-Medium.ttf")
            val custom_reg = Typeface.createFromAsset(context.assets, "fonts/Mada-Regular.ttf")
            binding.itemName.typeface=custom_med
            binding.price.typeface=custom_med
            binding.size.typeface=custom_med
            binding.color.typeface=custom_med
            binding.quantity.typeface=custom_reg

        }
    }


}

