package com.halem.sweply.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.halem.sweply.databinding.ItemsStoreLayoutBinding
import com.halem.sweply.fragments.store.model.LimitedProductsItem

class ItemsStoreAdapter (val onClickListener: ItemsStoreAdapter.OnClickListener) : PagedListAdapter<LimitedProductsItem, ItemsStoreAdapter.ViewHolder>(ItemsStoreAdapter) {
    companion object DiffCallback : DiffUtil.ItemCallback<LimitedProductsItem>() {
        override fun areItemsTheSame(oldItem: LimitedProductsItem, newItem: LimitedProductsItem): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: LimitedProductsItem, newItem: LimitedProductsItem): Boolean {
            return oldItem.id == newItem.id
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemsStoreLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val marsProperty = getItem(position)
        holder.bind(marsProperty!!)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(marsProperty)
        }
    }


    class ViewHolder(private var binding: ItemsStoreLayoutBinding):
        RecyclerView.ViewHolder(binding.root) {
        fun bind(storeItem: LimitedProductsItem) {
            binding.dataItem = storeItem
//            ("sizeItems",storeItem.limitedProducts?.size.toString())

            binding.executePendingBindings()
        }
    }


    class OnClickListener(val clickListener: (marsProperty: LimitedProductsItem) -> Unit) {
        fun onClick(marsProperty:LimitedProductsItem) = clickListener(marsProperty)
    }
}
