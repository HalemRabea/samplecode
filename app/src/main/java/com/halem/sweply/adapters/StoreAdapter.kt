/*
 * Copyright 2018, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.halem.sweply.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.halem.sweply.databinding.StoreLayoutBinding
import com.halem.sweply.fragments.store.StoreFragmentDirections
import com.halem.sweply.fragments.store.model.DataItem


class StoreAdapter(val onClickListener: OnClickListener) : ListAdapter<DataItem, StoreAdapter.StoreViewHolder>(
    DiffCallback
) {

    companion object DiffCallback : DiffUtil.ItemCallback<DataItem>() {
        override fun areItemsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
            return oldItem.id == newItem.id
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreViewHolder {
        return StoreViewHolder(
            StoreLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: StoreViewHolder, position: Int) {
        val marsProperty = getItem(position)
        holder.bind(marsProperty)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(marsProperty)
        }
    }


    class StoreViewHolder(private var binding: StoreLayoutBinding):
            RecyclerView.ViewHolder(binding.root) {
        fun bind(storeItem: DataItem) {
            binding.dataStores = storeItem
            binding.executePendingBindings()
        }
    }


    class OnClickListener(val clickListener: (marsProperty: DataItem) -> Unit) {
        fun onClick(marsProperty:DataItem) = clickListener(marsProperty)
    }
}

