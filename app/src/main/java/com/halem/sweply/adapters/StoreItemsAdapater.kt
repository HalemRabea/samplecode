package com.halem.sweply.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.halem.sweply.databinding.ItemLayoutBinding
import com.halem.sweply.fragments.store.model.LimitedProductsItem

class StoreItemsAdapater(val onClickListener: OnClickListener) : ListAdapter<LimitedProductsItem, StoreItemsAdapater.StoreViewHolder>(
    DiffCallback
) {

    companion object DiffCallback : DiffUtil.ItemCallback<LimitedProductsItem>() {
        override fun areItemsTheSame(oldItem: LimitedProductsItem, newItem: LimitedProductsItem): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: LimitedProductsItem, newItem: LimitedProductsItem): Boolean {
            return oldItem.id == newItem.id
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreViewHolder {
        return StoreViewHolder(
            ItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: StoreViewHolder, position: Int) {
        val marsProperty = getItem(position)
        holder.bind(marsProperty)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(marsProperty)
        }
    }


    class StoreViewHolder(private var binding: ItemLayoutBinding):
        RecyclerView.ViewHolder(binding.root) {
        fun bind(storeItem: LimitedProductsItem) {
            binding.dataItem = storeItem
            binding.executePendingBindings()
        }
    }


    class OnClickListener(val clickListener: (marsProperty: LimitedProductsItem) -> Unit) {
        fun onClick(marsProperty:LimitedProductsItem) = clickListener(marsProperty)
    }
}

