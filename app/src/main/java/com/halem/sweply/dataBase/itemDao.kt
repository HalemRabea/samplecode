package com.halem.sweply.dataBase

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.*
import com.halem.sweply.fragments.store.model.LimitedProductsItem
@Dao
interface itemDao {

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    fun insertNew(item: LimitedProductsItem)

    @Insert
    fun insert(item: LimitedProductsItem)

    @Update
    fun update(item: LimitedProductsItem)

    @Delete
    fun delete(item: LimitedProductsItem)

    @Query("SELECT * from item_cart WHERE id = :key")
    fun get(key: Int): LimitedProductsItem?


    @Query("DELETE FROM item_cart")
    fun clearAll()

    @Query("SELECT * FROM item_cart")
    fun getAllItems(): LiveData<List<LimitedProductsItem>>

}