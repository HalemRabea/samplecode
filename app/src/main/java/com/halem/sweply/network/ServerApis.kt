package com.halem.sweply.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
//import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit


private const val BASE_URL = ""


private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()


private fun makeOkHttpClient(): OkHttpClient {
    return OkHttpClient.Builder()
//        .addInterceptor(makeLoggingInterceptor())
        .connectTimeout(120, TimeUnit.SECONDS)
        .readTimeout(120, TimeUnit.SECONDS)
        .writeTimeout(90, TimeUnit.SECONDS)
        .build()
}

//private fun makeLoggingInterceptor(): HttpLoggingInterceptor {
//    val logging = HttpLoggingInterceptor()
//    logging.level =
//        HttpLoggingInterceptor.Level.BODY
//    return logging
//}

private val retrofit = Retrofit.Builder()
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .client(makeOkHttpClient())
    .build()


object Api {
    val retrofitService : ApiService by lazy {
        retrofit.create(ApiService::class.java)
    }
}