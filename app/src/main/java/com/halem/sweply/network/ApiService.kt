package com.halem.sweply.network

import com.halem.sweply.fragments.submitCart.model.modelCities.CitesModel
import com.halem.sweply.fragments.store.model.StoreModel
import com.halem.sweply.fragments.storeItems.model.StoreItemsModel
import com.halem.sweply.fragments.submitCart.model.modelSendSubmit.SubmitModelSend
import com.halem.sweply.fragments.submitCart.model.modelSubmit.ResponseSubmit
import kotlinx.coroutines.Deferred
import retrofit2.http.*


interface ApiService {
    @GET("/api/sections/get")
    fun getSections(): Deferred<StoreModel>

    @GET("/api/products/get?per_page=10")
    fun getItemsSection(@Query("section_id") IdStore:Int,@Query("page") page:Int): Deferred<StoreItemsModel>

    @GET("/api/cities/get")
    fun getCites(): Deferred<CitesModel>

    @POST("/api/orders/post-order")
    fun submitOrder(@Body data: SubmitModelSend): Deferred<ResponseSubmit>
}