package com.halem.sweply.utility

import com.halem.sweply.R


val rootDestinations = setOf(R.id.storeFragment, R.id.cart, R.id.about)

enum class NetworkApiStatus { LOADING, ERROR, DONE }